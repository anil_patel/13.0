{
    'name': 'Website Login/signup and checkout reCAPTCHA',
    'version': '1.0.1',
    'category': 'Website/Website',
    'summary': 'This module allows to verify user on Login, Signup, Checkout Address page.',
    'description': """This module allows to verify user on Login, Signup, Checkout Address page.""",
    'price': "25",
    'currency': 'USD',
    'depends': ['base', 'website','website_sale'],
    'images': ['static/description/main_screenshot.png'],
    "data": [
        'views/res_config_setting.xml',
        'views/templates.xml',
    ],
    'qweb': [],
    'installable': True,
    'auto_install': False,
}